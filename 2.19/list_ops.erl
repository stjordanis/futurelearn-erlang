-module(list_ops).
-include_lib("eunit/include/eunit.hrl").
-export([take/2,
         direct_take/2,
         nub/1,
         direct_nub/1,
         is_palindrome/1]).

%% Extract first N elements from list.
-spec take(integer(), [T]) -> [T].
take(N, L) ->
    lists:reverse(take(N, L, [])).
take(0, _, Acc) ->
    Acc;
take(_, [], Acc) ->
    Acc;
take(N, [H|T], Acc) when N > 0 ->
    take(N - 1, T, [H|Acc]).

%% Direct-recursion variant of take/2.
-spec direct_take(integer(), [T]) -> [T].
direct_take(0, _) ->
    [];
direct_take(_, []) ->
    [];
direct_take(N, [H|T]) when N > 0 ->
    [H|direct_take(N-1, T)].

%% Turn list into set.
-spec nub([T]) -> [T].
nub(L) ->
    lists:reverse(nub(L, [])).
nub([], Acc) ->
    Acc;
nub([H|T], Acc) ->
    nub(remove(H, T), [H|Acc]).

%% Direct-recursion variant of nub/1.
-spec direct_nub([T]) -> [T].
direct_nub([]) ->
    [];
direct_nub([H|T]) ->
    [H|direct_remove(T, H)].

%% Remove all instances of an element from list.
-spec remove(T, [T]) -> [T].
remove(E, L) ->
    lists:reverse(remove(E, L, [])).
remove(_, [], Acc) ->
    Acc;
remove(E, [E|T], Acc) ->
    remove(E, T, Acc);
remove(E, [H|T], Acc) ->
    remove(E, T, [H|Acc]).

%% Direct-recursion variant of remove/2.
-spec direct_remove(T, [T]) -> [T].
direct_remove(_, []) ->
    [];
direct_remove(E, [E|T]) ->
    direct_remove(E, T);
direct_remove(E, [H|T]) ->
    [H|direct_remove(E, T)].

%% Check if string is palindromic.
-spec is_palindrome([_]) -> true | false.
is_palindrome(L) ->
    %% Remove punctuation and change all to lowercase.
    M = lowercase(filter_letters(L)),
    Len = length(M),
    Half = Len div 2,
    %% Check that each side of string is the same, missing central character if relevant.
    %% Or use string:equal/3
    take(Half, M, []) == lists:nthtail(Half + Len rem 2, M).

%% Map list of ASCII letters into lowercase ASCII letters.
-spec lowercase([integer()]) -> [integer()].
lowercase([]) ->
    [];
lowercase([H|T]) ->
    [H bor 32|lowercase(T)].

%% Map list into list of ASCII letters.
-spec filter_letters([integer()]) -> [integer()].
filter_letters([]) ->
    [];
filter_letters([H|T]) ->
    case is_letter(H) of
        true -> [H|filter_letters(T)];
        false -> filter_letters(T)
    end.

%% Check character is ASCII letter.
-spec is_letter(integer()) -> true | false.
is_letter(N) ->
    (65 =< N andalso 90 >= N) orelse (97 =< N andalso 122 >= N).

take_test() ->
    ?assertError(function_clause, take(-1, [0])),
    ?assertEqual([], take(0, [])),
    ?assertEqual([], take(1, [])),
    ?assertEqual([], take(0, [0])),
    ?assertEqual([0], take(1, [0])),
    ?assertEqual([0], take(2, [0])),
    ?assertEqual([0, 1], take(2, [0, 1])).

nub_test() ->
    ?assertEqual([], nub([])),
    ?assertEqual([0], nub([0])),
    ?assertEqual([0], nub([0, 0])),
    ?assertEqual([0, 1, 2], nub([0, 1, 0, 2])).

remove_test() ->
    ?assertEqual([], remove(0, [])),
    ?assertEqual([], remove(0, [0])),
    ?assertEqual([0], remove(1, [0])),
    ?assertEqual([1], remove(0, [0, 1])),
    ?assertEqual([1, 2, 3], remove(0, [0, 1, 2, 3])).

is_palindrome_test() ->
    ?assert(is_palindrome("abccba")),
    ?assert(is_palindrome("racecar")),
    ?assert(is_palindrome("Madam, I'm Adam")),
    ?assertNot(is_palindrome("palindrome")).

filter_letters_test() ->
    ?assertEqual("", filter_letters("`1234567890[]',./=-\;~!@#$%^&*(){}?+_|:")),
    ?assertEqual("pyfgcrlaoeuidhtnsqjkxbmwvz"
                 ++ "PYFGCRLAOEUIDHTNSQJKXBMWVZ",
                 filter_letters("pyfgcrlaoeuidhtnsqjkxbmwvz"
                                ++ "PYFGCRLAOEUIDHTNSQJKXBMWVZ")).

is_letter_test() ->
    F = fun(E) -> ?assert(is_letter(E)) end,
    lists:foreach(F, "pyfgcrlaoeuidhtnsqjkxbmwvz"
                  ++ "PYFGCRLAOEUIDHTNSQJKXBMWVZ").

lowercase_test() ->
    ?assertEqual("pyfgcrlaoeuidhtnsqjkxbmwvz" ++
                     "pyfgcrlaoeuidhtnsqjkxbmwvz",
                 lowercase("pyfgcrlaoeuidhtnsqjkxbmwvz"
                           ++ "PYFGCRLAOEUIDHTNSQJKXBMWVZ")).

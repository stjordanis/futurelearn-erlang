-module(list_funs).
-include_lib("eunit/include/eunit.hrl").
-export([product/1,
         direct_product/1,
         maximum/1,
         direct_maximum/1]).

product(L) ->
    product(L, 1).
product([], Acc) ->
    Acc;
product([X|Xs], Acc) ->
    product(Xs, X*Acc).

direct_product([]) ->
    1;
direct_product([X|Xs]) ->
    X * direct_product(Xs).

maximum([X|Xs]) ->
    maximum(Xs, X).
maximum([], Max) ->
    Max;
maximum([X|Xs], Max) ->
    maximum(Xs, max(X, Max)).

direct_maximum([Max]) ->
    Max;
%% Thought I'd use guards here for no reason.
direct_maximum([First, Second|Rest]) when First >= Second ->
    direct_maximum([First|Rest]);
direct_maximum([First, Second|Rest]) when Second > First ->
    direct_maximum([Second|Rest]).

product_test() ->
    ?assertEqual(1, product([])),
    ?assertEqual(1, product([1])),
    ?assertEqual(24, product([1, 2, 3, 4])),
    ?assertEqual(24, product([4, 3, 2, 1])),
    ?assertEqual(1, direct_product([])),
    ?assertEqual(1, direct_product([1])),
    ?assertEqual(24, direct_product([1, 2, 3, 4])),
    ?assertEqual(24, direct_product([4, 3, 2, 1])).

maximum_test() ->
    ?assertError(function_clause, maximum([])),
    ?assertEqual(1, maximum([1])),
    ?assertEqual(4, maximum([1, 2, 3, 4])),
    ?assertEqual(4, maximum([4, 3, 2, 1])),
    ?assertError(function_clause, direct_maximum([])),
    ?assertEqual(1, direct_maximum([1])),
    ?assertEqual(4, direct_maximum([1, 2, 3, 4])),
    ?assertEqual(4, direct_maximum([4, 3, 2, 1])).

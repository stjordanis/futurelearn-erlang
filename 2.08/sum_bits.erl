-module(sum_bits).
-export([bits/1, direct_bits/1, list_bits/1, test/0]).

%% Use remainder method
bits(N) ->
    bits(N, 0).
bits(0, Acc) ->
    Acc;
bits(N, Acc) when N >= 0 ->
    bits(N div 2, N rem 2 + Acc).

%% Direct-recursive method
direct_bits(0) ->
    0;
direct_bits(N) when N >= 0 ->
    N rem 2 + direct_bits(N div 2).

%% Alternative method by exploiting strings being lists
list_bits(N) ->
    %% Convert integer to string representation in base-2
    %% Then count amount of 1s
    lists:foldl(fun(X, NBits) ->
                        case X of
                            $1 ->
                                NBits + 1;
                            _ ->
                                NBits
                        end
                end,
                0, integer_to_list(N, 2)).

assert(N, NBits) ->
    NBits = bits(N),
    NBits = direct_bits(N),
    NBits = list_bits(N).

test() ->
    assert(0, 0),
    assert(1, 1),
    assert(2, 1),
    assert(3, 2),
    assert(4, 1),
    assert(5, 2),
    assert(6, 2),
    assert(7, 3),
    assert(8, 1),
    assert(9, 2),
    assert(10, 2),
    assert(11, 3),
    assert(12, 2),
    assert(13, 3),
    assert(14, 3),
    assert(15, 4),
    assert(16, 1),
    assert(17, 2),
    assert(18, 2),
    assert(19, 3),
    assert(20, 2),
    assert(21, 3),
    assert(22, 3),
    assert(23, 4),
    assert(24, 2),
    assert(25, 3),
    assert(26, 3),
    assert(27, 4),
    assert(28, 3),
    assert(29, 4),
    assert(30, 4),
    assert(31, 5),
    assert(32, 1),
    ok.

-module(tail_fibonacci).
-export([fib/1]).

fib(N) ->
    fib(N, 0, 1).

fib(0, Curr, _) ->
    Curr;
fib(N, Curr, Next) when N > 0 ->
    fib(N - 1, Next, Curr + Next).
